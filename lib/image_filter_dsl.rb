require_relative './image_filter_dsl/dsl/filter.rb'
require_relative './image_filter_dsl/dsl/filter_instructions.rb'
require_relative './image_filter_dsl/dsl/kernel.rb'
require_relative './image_filter_dsl/binary/struct.rb'
require_relative './image_filter_dsl/binary/serialize.rb'
require_relative './image_filter_dsl/engine/io.rb'
require_relative './image_filter_dsl/engine/image_processor.rb'
require_relative './image_filter_dsl/serialize.rb'
require_relative './image_filter_dsl/engine/cli.rb'
##
# Image Filter DSL Library
# (c) 2018-2020 VDTDEV/Wade H. ~ MIT License
# @author Wade H. <vdtdev.prod@gmail.com>
module ImageFilterDsl

    ##
    # Reference to Filter module
    Filter = Dsl::Filter

    ##
    # Shortcut to ImageProcessor constructor
    # @param [Dsl::Kernel::FilterKernel|String] kernel FilterKernel object or path to binary kernel file
    # @param [Integer] threads How many threads for processor to use
    # @return [Engine::ImageProcessor] new instance of image processor
    def self.image_processor(kernel, threads=6)
        Engine::ImageProcessor.new(kernel, threads)
    end

    ##
    # Execute process image using kernel from CLI
    # @param [Array] args Array of arguments (kernel file, img in, img out)
    def self.cli_process_image(args)
        kernel_file = args[0]
        img_in = args[1]
        img_out = args[2]
        ip = image_processor(kernel_file)
        ip.process_image(img_in, img_out)
    end

    ##
    # Shorthand for Engine::IO.write
    # @param [Dsl::Kernel::FilterKernel] filter Filter to write
    # @param [String] filename Filename to write to
    def self.save_binary_kernel(filter, filename)
        Engine::IO.write(filename, filter)
    end

end