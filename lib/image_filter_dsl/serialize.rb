require_relative './serializers/common.rb'
require_relative './serializers/msg_pack.rb'
require_relative './serializers/json.rb'
require_relative './serializers/yaml.rb'
require_relative './serializers/intermediate.rb'
##
# Image Filter DSL Library
# (c) 2018-2020 VDTDEV/Wade H. ~ MIT License
# @author Wade H. <vdtdev.prod@gmail.com>
module ImageFilterDsl
    ##
    # Module providing access to kernel serialization and 
    # deserialization functionality. Actual serialization/deserialization
    # logic defined in modules within `Serializers`
    module Serialize
        extend self
        # include Serialize::Json
        # include Serializers::Json

        ##
        # Intermediate serializer module
        INTERMEDIATE = Serializers::Intermediate
        
        ##
        # Available serializers, mapping format symbol to
        # module
        SERIALIZERS = {
            json: Serializers::Json,
            msgpack: Serializers::MsgPack,
            yaml: Serializers::Yaml
        }
        ##
        # Available serializer symbols
        FORMATS = SERIALIZERS.keys

        ##
        # Serialize kernel, optionally using intermediate step
        # @param [ImageFilterDsl::Dsl::Kernel] kernel Kernel
        # @param [Symbol] format Format to serialize to from FORMATS, or
        #   `nil` to convert to intermediate
        # @return [Hash|String] serialized kernel 
        def from_kernel(kernel, format=nil)
            i = INTERMEDIATE.from_kernel(kernel)
            if format.nil?
                return i
            else
                m = SERIALIZERS[format]
                return m.send(:serialize, i)
            end
        end

        ##
        # Deserialize to kernel, optionally using intermediate step
        # @param [Hash|String] data Serialized data
        # @param [Symbol] format Format to convert from from FORMATS, or
        #   `nil` to convert from intermediate
        # @return [Kernel] Kernal from deserialization
        def to_kernel(data,format=nil)
            if format.nil?
                return INTERMEDIATE.to_kernel(data)
            else
                i = SERIALIZERS[format].send(:deserialize, data)
                return INTERMEDIATE.to_kernel(i)
            end
        end        
    end
end
            

