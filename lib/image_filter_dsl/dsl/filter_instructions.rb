##
# Image Filter DSL Library
# (c) 2018-2020 VDTDEV/Wade H. ~ MIT License
# @author Wade H. <vdtdev.prod@gmail.com>
module ImageFilterDsl
    ##
    # Module defining DSL
    module Dsl
        ##
        # Module defining filter kernel instruction logic
        module FilterInstructions

            ##
            # Hash of binary instruction symbols
            OP_INS = {
                # math = 0x0X-0xAF
                add:    0x01,
                mult:   0x02,
                div:    0x03,
                mod:    0x04,
                abs:    0x05,
                # collection op = 0xB0-0xCF
                min:    0xb0,
                max:    0xb1,
                avg:    0xb2,
                # logic/mem/conv op = 0xD0-??
                copy:   0xd0,
                above:  0xd1,
                below:  0xd2,
                floor:  0xd3,
                ceil:   0xd4,
                float:  0xd5,
                round:  0xd6,
                switch: 0xd7,
                eq:     0xd8,
                bnot:   0xd9,
                mix:    0xda,
                between: 0xdb,
                clamp:  0xdc,
                band:   0xdd,
                bor:    0xde,
                # generation
                rand:   0xf0,
                sin:    0xf1,
                cos:    0xf2,
                tan:    0xf3
            }

            ##
            # Array of all valid filter instructions
            OPS = OP_INS.keys
        
            ##
            # Add instruction
            # @param [Array] i input values
            # @return [Integer|Float] output value
            def self.add(i) 
                i.sum
            end
        
            ##
            # Multiply instruction
            # @param [Array] i input values
            # @return [Integer|Float] output value
            def self.mult(i)
                v=1;
                i.each{|n|v=v*n}
                v
            end

            ##
            # Multiply instruction
            # @param [Array] i input values
            # @return [Integer|Float] output value
            def self.div(i)
                i[0]/i[1]
            end
            ##
            # Calculate modulo
            # @param [Array] i input values
            # @return [Integer|] output value
            def self.mod(i)
                i[0] % i[1]
            end

            ##
            # Absolute value
            # @param [Array] i input value
            # @return [Integer|Float] output value
            def self.abs(i)
                i[0].abs
            end

            ##
            # Minimum instruction
            # @param [Array] i input values
            # @return [Integer|Float] output value
            def self.min(i)
                i.min
            end
        
            ##
            # Maximum instruction
            # @param [Array] i input values
            # @return [Integer|Float] output value
            def self.max(i)
                i.max
            end
        
            ##
            # Average instruction
            # @param [Array] i input values
            # @return [Integer|Float] output value
            def self.avg(i)
                i.sum / (1.0 * i.length)
            end
        
            ##
            # Copy instruction
            # @param [Array] i input values (src)
            # @return [Integer|Float] output value
            def self.copy(i)
                i[0]
            end
        
            ##
            # Above instruction
            # @param [Array] i input values `(a,b,trueVal,falseVal)`
            #   `trueVal` defaults to `1`, `falseVal` defaults to `0`
            # @return [Integer|Float] output value
            def self.above(i)
                i = [i,1,0].flatten if i.length == 2
                if(i[0]>i[1])
                    if i.length < 3
                        1
                    else
                        i[2]
                    end
                else
                    if i.length < 4
                        0
                    else
                        i[3]
                    end
                end
            end
        
            ##
            # Below instruction
            # @param [Array] i input values `(a,b,trueVal,falseVal)`
            #   `trueVal` defaults to `1`, `falseVal` defaults to `0`
            # @return [Integer|Float] output value
            def self.below(i)
                i = [i,1,0].flatten if i.length == 2
                if(i[0]<i[1])
                    if i.length < 3
                        1
                    else
                        i[2]
                    end
                else
                    if i.length < 4
                        0
                    else
                        i[3]
                    end
                end
            end

            ##
            # Between instruction (check if value is between two others)
            # @param [Array] i input values `[min, max, value, trueVal, falseVal]`
            #   `trueVal` defaults to `1`, `falseVal` defaults to `0`
            # @return [Integer] 1 if true, 0 if false
            def self.between(i)
                i = [i,1,0].flatten if i.length == 3
                a,b,v,t,f = i
                r = (v>=a) && (v<=b)
                (r)? t : f
            end

            ##
            # Clamp value between a min and a max
            # @param [Array] i input values `[min,max,val]`
            # @return [Integer|Float] Value forced to be no greater than `max`
            #   and no less than `min`
            def self.clamp(i)
                a,b,v = i
                [b,[a,v].max].min
            end

            ##
            # Floor instruction
            # @param [Array] i input value (v)
            # @return [Integer|Float] output value
            def self.floor(i)
                i[0].floor
            end

            ##
            # Ceil instruction
            # @param [Array] i input value (v)
            # @return [Integer|Float] output value
            def self.ceil(i)
                i[0].ceil
            end

            ##
            # Float cast instruction
            # @param [Array] i input value (v)
            # @return [Integer|Float] output value
            def self.float(i)
                i[0].to_f
            end

            ##
            # Round instruction
            # @param [Array] i input values (val, decimal_places)
            # @return [Integer|Float] output value
            def self.round(i)
                i[0].round(i[1])
            end

            ##
            # 'Switch' instruction (basically if)
            #
            # switch condition (0/1/0.0/1.0), trueval, falseval]
            # @param [Array] i input value (condition, true val, false val)
            # @return [Integer|Float] output value
            def self.switch(i)
                if i[0].to_i == 1
                    i[1]
                else
                    i[2]
                end
            end

            ##
            # Equal condition instruction
            # @param [Array] i input values (a, b)
            # @return [Integer|Float] output value 1 true 0 falsew
            def self.eq(i)
                if i[0] == i[1]
                    1
                else
                    0
                end
            end

            ##
            # Logic invert  instruction
            # @param [Array] i input value (0 or 1)
            # @return [Integer|Float] output value (1 if in 0, 0 if in 1)
            def self.bnot(i)
                if i[0].to_i == 1
                    0
                else
                    1
                end
            end

            ##
            # Logical AND instruction
            # @param [Array] i input values (ints or floats)
            # @return [Integer] 1 if all values are `1` or `1.0`, else 0
            def self.band(i)
                (i.reduce(:+).to_i == i.length)? 1 : 0
            end

            ##
            # Logical OR instruction
            # @param [Array] i input values (ints or floats)
            # @return [Integer] 1 if any values are `1` or `1.0`, else 0
            def self.bor(i)
                (i.reduce(:+).to_i > 0)? 1 : 0
            end

            ##
            # Mix two values with a ratio
            # @param [Array] i input values (ratio (0-1.0), a, b)
            # @return [Float] output value of ((ratio*a) + ((1.0-ratio)*b))
            def self.mix(i)
                ratio, a, b = i
                (ratio*a)+((1.0-ratio)*b)
            end

            ##
            # Random number instruction
            # @param [Array] i input values (min, max)
            # @return [Float] Random number between min and max
            def self.rand(i)
                r=Random.new
                min,max = i
                (-min) + (r.rand * (max + min))
            end

            ##
            # Sine function
            # @param [Array] i input value to use sin on
            # @return [Float] Sine of input value
            def self.sin(i)
                Math.sin(i[0])
            end

            ##
            # Cosine function
            # @param [Array] i input value to use cos on
            # @return [Float] Cosine of input value
            def self.cos(i)
                Math.cos(i[0])
            end

            ##
            # Tangent function
            # @param [Array] i input value to use tan on
            # @return [Float] Tangent of input value
            def self.tan(i)
                Math.tan(i[0])
            end
        end
    end
end
