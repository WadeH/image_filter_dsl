##
# Image Filter DSL Library
# (c) 2018-2020 VDTDEV/Wade H. ~ MIT License
# @author Wade H. <vdtdev.prod@gmail.com>
module ImageFilterDsl
    module Dsl
        ##
        # Module used for declaring Filter w/ DSL
        module Filter

            ##
            # Define method
            # @param [Array] ins Input symbols
            # @param [Array] outs Output symbols
            # @param [Proc] block Filter instructions body
            # @return [FilterKernel] new Filter Kernel
            def self.define(ins,outs,&block)
                kernel = Kernel::FilterKernel.new(ins,outs)
                ip = processor(kernel)
                ip.instance_eval &block
                return kernel
            end
        
            private

            ##
            # Process filter, creating new Instruction Processing module
            # @param [FilterKernel] kernel Target FilterKernel
            # @return [Module] module wrapping instructions to target kernel
            def self.processor(kernel)
                instruction_proc = Module.new
        
                FilterInstructions::OPS.each do |op|
                    p = Proc.new do |ins,outs|
                        kernel.store_instruction(
                            Kernel::KernelInstruction.new(op,ins,outs)
                        )
                    end
                    instruction_proc.class.send(:define_method, op, p)
                end
        
                instruction_proc
            end
    
        end
    end
end