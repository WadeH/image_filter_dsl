##
# Image Filter DSL Library
# (c) 2018-2020 VDTDEV/Wade H. ~ MIT License
# @author Wade H. <vdtdev.prod@gmail.com>
module ImageFilterDsl
    module Dsl
        ##
        # Kernal module
        module Kernel
            ##
            # IFD Filter Kernel class
            # @author vdtdev <vdtdev@gmail.com>
            class FilterKernel
                attr_accessor :instructions
                attr_accessor :inputs
                attr_accessor :outputs

                ##
                # Kernel constructor
                # @param [Array] inputs Input symbols
                # @param [Array] outputs Output symbols
                def initialize(inputs=[],outputs=[])
                    @inputs = inputs
                    @outputs = outputs
                    @instructions = []
                end

                ##
                # Store instruction in kernel
                # @param [Kernel::KernelInstruction] instruction Instruction to add
                # @return [Integer] total number of instructions
                def store_instruction(instruction)
                    @instructions.append(instruction)
                    @instructions.length
                end

                ##
                # Process filter kernel and produce output hash
                # @param [Hash] initial_values Hash of values for inputs
                # @return [Hash] hash of result values for output symbols
                def process(initial_values)
                    outs = Hash[* @outputs.map{|o| [o,nil] }.flatten]
                    @instructions.each do |i|
                        v = i.calculate(initial_values)
                        if @outputs.include?(i.out)
                            outs[i.out] = v
                        else
                            initial_values[i.out] = v
                        end
                    end
                    outs
                end
            end

            ##
            # Class representing a FilterKernel instruction
            class KernelInstruction
                attr_accessor :op
                attr_accessor :inputs
                attr_accessor :out

                ##
                # Construct new Filter Kernel Instruction
                # @param [Symbol] op Instruction operation
                # @param [Array] ins Input symbols
                # @param [Symbol] out Output symbol
                def initialize(op,ins,out)
                    @op = op
                    @inputs = ins
                    unless @inputs.kind_of?(Array)
                        @inputs = [@inputs]
                    end
                    @out = out
                end

                ##
                # Calculate instruction output
                # @param [Hash] variables Hash of variable values
                # @return [Integer|Float] output value
                def calculate(variables)
                    vals = @inputs.map{|v| (v.kind_of?(Symbol))? variables[v] : v }
                    FilterInstructions.method(@op).call(vals)
                end
            end
        end
    end
end