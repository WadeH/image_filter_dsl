require 'chunky_png'

##
# Image Filter DSL Library
# (c) 2018-2020 VDTDEV/Wade H. ~ MIT License
# @author Wade H. <vdtdev.prod@gmail.com>
module ImageFilterDsl
    # Module containing processing engine related code
    module Engine
        ##
        # Image Processor class applies Filter Kernels to images
        class ImageProcessor

            attr_accessor :kernel, :config



            ##
            # Constructor
            # @param [Dsl::Kernel::FilterKernel|String] filter_kernel FilterKernel object or path to binary kernel file
            # @param [Integer] thread_count How many threads for processor to use
            def initialize(filter_kernel, thread_count=6)
                @config = {
                    filter_kernel_filename: nil,
                    threads: thread_count
                }

                if filter_kernel.is_a?(String)
                    @config[:filter_kernel_filename] = filter_kernel

                    @kernel = Engine::IO.read(
                        filter_kernel,
                        Engine::IO::DataFormat::KERNEL
                    )
                else
                    @kernel = filter_kernel
                end
            end

            ##
            # Process image
            # @param [String] img_src_filename Source image filename
            # @param [String] img_out_filename Output image filename
            def process_image(img_src_filename, img_out_filename)
                img = load_image(img_src_filename)

                size = [img[:image].width, img[:image].height]
                p_count = size[0] * size[1]

                p_part = p_count / @config[:threads]
                thread_loads = [].fill(p_part, (0..(@config[:threads])-1))

                if (p_part * @config[:threads]) < p_count
                    thread_loads[-1] += (p_count - (p_part * @config[:threads]))
                end

                ranges = thread_loads.map.with_index do |l,i|
                    start = thread_loads[0..i][0..-2].sum
                    fin = start + (l - 1)
                    (start..fin)
                end

                ranges = ranges.map{|r| r.map{|i| i }}

                threads = []

                (0..@config[:threads]-1).each do |i|
                    threads << Thread.new {
                        run_process(img, ranges[i])
                    }
                end

                threads.each { |th| th.join }

                save_image(img, img_out_filename)
            end

            private

            ##
            # Convert point index to x,y coord
            # @param [Integer] p Point index
            # @param [Integer] width Grid width
            # @return [Hash] Coordinate {:x, :y}
            def point_coord(p,width)
                {
                    x: (p % width),
                    y: (p / width)
                }
            end

            ##
            # Execute processing for point range
            # Called inside Thread.new
            # @param [Hash] data Image data
            # @param [Array] range Array of range points
            def run_process(data, range)

                range.each do |p|
                    c = point_coord(p, data[:image].width)
                    process_pixel(data, c[:x], c[:y])
                end

            end

            ##
            # Process pixel using filter
            # @param [Hash] data Image data hash
            # @param [Integer] x Source (possibly dest) X coordinate
            # @param [Integer] y Source (possibly dest) Y coordinate
            # @return [Hash] Copy of filter outputs
            def process_pixel(data, x, y)
                clr = ChunkyPNG::Color
                pixel = data[:image].get_pixel(x,y)

                # r = (clr.r(pixel)/255.0).round(2)
                # g = (clr.g(pixel)/255.0).round(2)
                # b = (clr.b(pixel)/255.0).round(2)
                # a = (clr.a(pixel)/255.0).round(2)

                r = clr.r(pixel)
                g = clr.g(pixel)
                b = clr.b(pixel)
                a = clr.a(pixel)
                
                inputs = Hash[*@kernel.inputs.map{|k|[k,0]}.flatten]
                inputs[:x] = x if inputs.keys.include?(:x)
                inputs[:y] = y if inputs.keys.include?(:y)
                inputs[:r] = r if inputs.keys.include?(:r)
                inputs[:g] = g if inputs.keys.include?(:g)
                inputs[:b] = b if inputs.keys.include?(:b)
                inputs[:a] = a if inputs.keys.include?(:a)
                inputs[:width] = data[:image].width if inputs.keys.include?(:width)
                inputs[:hght] = data[:image].height if inputs.keys.include?(:hght)


                o = @kernel.process(inputs)

                outputs = {
                    x: x, y: y, r: r, g: g, b: b, a: a
                }

                o.keys.each do |k|
                    if o[k] == Float::INFINITY
                        p "Infinity: #{k}"
                    end
                    outputs[k] = o[k].to_i
                end

                # o_color = clr.rgba(
                #     (255*outputs[:r]).to_i,
                #     (255*outputs[:g]).to_i,
                #     (255*outputs[:b]).to_i,
                #     (255*outputs[:a]).to_i
                # )

                o_color = clr.rgba(
                    outputs[:r].to_i,
                    outputs[:g].to_i,
                    outputs[:b].to_i,
                    outputs[:a].to_i
                )

                data[:image].set_pixel(
                    outputs[:x], outputs[:y],
                    o_color
                )
            end

            ##
            # Load PNG image using ChunkyPNG
            # @param [String] filename Filename of image to load
            # @return [Hash] Hash with :stream as ChunkyPNG::Datastream and
            #   :image as ChunkyPNG::Image and :filename with original filename
            def load_image(filename)
                stream = ChunkyPNG::Datastream.from_file(filename)
                image = ChunkyPNG::Image.from_datastream(stream)
                
                return {
                    filename: filename,
                    stream: stream,
                    image: image
                }
            end

            ##
            # Save image data to disk as PNG
            # @param [Hash] data Image data as provided by load_image
            # @param [String] filename Optional filename to save to; if none given,
            #   uses the original filename from data hash
            # @return [File] file object written to
            def save_image(data, filename = nil)
                # If no filename given, use original
                filename = data[:filename] if filename.nil?
                stream = data[:image].to_datastream()
                stream.save(filename)
            end

        end
    end
end
