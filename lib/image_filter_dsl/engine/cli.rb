##
# Image Filter DSL Library
# (c) 2018-2020 VDTDEV/Wade H. ~ MIT License
# @author Wade H. <vdtdev.prod@gmail.com>
module ImageFilterDsl
    module Engine
        ##
        # CLI Argument parser and handler module
        module CLI
            extend self

            Options = Struct.new(:action, :filter, :input, :output)
            RequiredArgs = {
                process: [:filter, :input, :output],
                compile: [:input, :output]
            }

            def parse(args)
                opts = Options.new
                args=args.map{|s|s.strip}

                p = args[0]
                
                if !p.nil? && p.downcase == '--process'
                    opts.action = :process
                elsif !p.nil? && p.downcase == '--compile'
                    opts.action = :compile
                end

                unless opts.action.nil?
                    case opts.action
                    when :process
                        f,i,o = args[1..-1]
                        opts.filter = f
                        opts.input = i
                        opts.output = o
                    when :compile
                        s,k = args[1..-1]
                        opts.input = s
                        opts.output = k
                    end
                end

                return opts
            end

            def run()
                options = parse(ARGV)
                if options.action.nil?
                    show_usage
                else
                    p options
                    if RequiredArgs[options.action].select{|o| options[o].nil?}.length == 0
                        do_action(options)
                    else
                        show_usage(options.action)
                    end
                end
            end

            def show_usage(action=nil)
                if !action.nil?
                    print "Invalid arguments for #{action.to_s}\n"
                end
                print [
                    "Usage: image_filter_dsl <action<",
                    "\t--process <kernel_file> <image in> <image out>\t\tProcess image with binary kernel",
                    "\t--compile <source_file> <output_kernel>\t\tCompile given ruby source to binary kernel"
                ].join("\n")
                print "\n"
            end

            def do_action(opts)
                if opts.action == :process
                    ip = ImageFilterDsl.image_processor(opts.filter)
                    ip.process_image(opts.input, opts.output)
                elsif opts.action == :compile
                    print "(Not yet implemented :)\n"
                end
            end
        end
    end
end