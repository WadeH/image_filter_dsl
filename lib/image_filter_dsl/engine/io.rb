##
# Image Filter DSL Library
# (c) 2018-2020 VDTDEV/Wade H. ~ MIT License
# @author Wade H. <vdtdev.prod@gmail.com>
module ImageFilterDsl
    module Engine
        ##
        # I/O methods for reading/writing Filter Kernel data to files
        module IO

            ##
            # Constants for format option used in read
            # Dictates what is returned
            module DataFormat
                ##
                # Filter Kernal instance
                KERNEL = :kernel
                ##
                # Filter kernal record
                RECORD = :record
            end

            ##
            # Read Filter Kernel from file
            # @param [String] filename Filename to read from
            # @param [Symbol] format Format to return (constants from
            #   DataFormat module; KERNEL or RECORD)
            # @return [Dsl::Kernel::FilterKernel|Binary::Struct::IfdKernel] loaded data, 
            #   in specified format
            def self.read(filename, format=DataFormat::KERNEL)
                record = Binary::Struct::IfdKernel.new
                File.open(filename, 'rb'){|f| record.read(f) }

                if format == DataFormat::RECORD
                    return record
                else
                    return Binary::Serialize.to_kernel(record)
                end
            end

            ##
            # Write filter kernel to file
            # @param [String] filename File name to write to
            # @param [Binary::Struct::IfdKernel|Dsl::Kernel::FilterKernel] filter Filter kernel or record to write
            # @return [Binary::Struct::IfdKernel] IfdKernel record
            def self.write(filename, filter)
                rec = nil
                if filter.is_a?(Dsl::Kernel::FilterKernel)
                    rec = Binary::Serialize.to_record(filter)
                elsif filter.is_a?(Binary::Struct::IfdKernel)
                    rec = filter
                end

                File.open(filename, 'wb') { |f| rec.write(f) }
            end

        end
    end
end