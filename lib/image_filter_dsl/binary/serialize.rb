##
# Image Filter DSL Library
# (c) 2018-2020 VDTDEV/Wade H. ~ MIT License
# @author Wade H. <vdtdev.prod@gmail.com>
module ImageFilterDsl
    ##
    # Module containing binary kernal related logic
    module Binary
        ##
        # Module providing serialization functionality for converting between
        # FilterKernel instance and binary IfdKernel record
        module Serialize

            # @!group Object -> Binary

            ##
            # Alias for Serialize::from_kernel
            def self.to_record(filter)
                from_kernel(filter)
            end

            ##
            # Build binary IfdKernel record from Kernel object
            # @param [Dsl::Kernel::FilterKernel] filter Source filter kernel
            # @return [Binary::Struct::IfdKernel] Binary kernel record
            def self.from_kernel(filter)
                variables = {
                    inputs: nil,
                    outputs: nil,
                    pool: nil,
                    symbols: nil
                }

                parts = {}

                # Collect variables
                pool = (filter.inputs + filter.outputs).uniq
                filter.instructions.each do |i|
                    pool += i.inputs
                    pool += [i.out]
                    pool.uniq!
                end
                variables[:pool] = pool

                # Generate variable symbols
                variables[:symbols] = {}
                count = 1
                variables[:pool].each do |v|
                    variables[:symbols][v] = count
                    count += 1
                end

                # Build variables record

                vars_rec = Binary::Struct::IfdVariables.new(
                    field_count: variables[:pool].length,
                    input_count: filter.inputs.length,
                    output_count: filter.outputs.length,
                    input_fields: filter.inputs.map { |i| variables[:symbols][i] },
                    output_fields: filter.outputs.map { |i| variables[:symbols][i] }
                )

                fkinds = Binary::Struct::FIELD_KINDS

                defs = variables[:symbols].keys.map do |k|
                    f_field = Binary::Struct::IfdField.new(
                        symbol: variables[:symbols][k]
                    )

                    if k.is_a?(Symbol)
                        f_field.kind = fkinds[:var]
                        f_field.name_length = k.to_s.length
                        f_field.variable.assign(k.to_s)
                    elsif k.is_a?(Integer)
                        f_field.kind = fkinds[:literal_int]
                        f_field.int_value = k
                    else
                        f_field.kind = fkinds[:literal_float]
                        f_field.float_value = k
                    end

                    f_field
                end

                vars_rec.definition = defs
                parts[:variables] = vars_rec

                # Build instructions records
                parts[:instructions] = filter.instructions.map do |i|
                    inp_fields = i.inputs.map { |f| variables[:symbols][f] }
                    f_ins = Binary::Struct::IfdInstruction.new(
                        operation: Binary::Struct.instruction_symbol(i.op),
                        input_count: i.inputs.length,
                        input_fields: inp_fields,
                        output_field: variables[:symbols][i.out]
                    )
                    f_ins
                end

                # Build Kernel Record
                kernel = Binary::Struct::IfdKernel.new(
                    header: Binary::Struct::IfdHeader.new,
                    variables: parts[:variables],
                    instruction_count: filter.instructions.length,
                    instructions: parts[:instructions]
                )

                return kernel
            end

            # @!endgroup

            # @!group Binary -> Object

            ##
            # Alias for Serialize::from_record
            def self.to_kernel(record)
                from_record(record)
            end

            ##
            # Convert IfdKernel record to FilterKernel object
            # @param [Binary::Struct::IfdKernel] record Record to convert
            # @return [Dsl::Kernel::FilterKernel] FilterKernel from record
            def self.from_record(record)
                ins = []
                outs = []

                vars = record.variables
                vardefs = vars.definition

                vars_val = Proc.new do |vs,force_string=false|
                    r = nil
                    i = vardefs.select{|v| v.symbol == vs}[0]
                    if i.kind == Binary::Struct::FIELD_KINDS[:var]
                        r = i.variable.strip.to_sym
                        r = ":#{r.to_s}" if force_string
                    elsif i.kind == Binary::Struct::FIELD_KINDS[:literal_int]
                        r = i.int_value
                    else
                        r = i.float_value
                    end
                    r
                end

                op_sym = Proc.new do |opc|
                    Dsl::FilterInstructions::OP_INS.select{|k,v| v==opc}.keys[0]
                end

                ins = vars.input_fields.map { |f|
                    vardefs.select{|v| v.symbol == f}[0].variable.strip.to_sym
                }

                outs = vars.output_fields.map { |f|
                    vardefs.select{|v| v.symbol == f}[0].variable.strip.to_sym
                }

                instructions = record.instructions.map do |i|
                    [
                        "#{op_sym.call(i.operation)} ",
                        i.input_fields.map{|f| vars_val.call(f) }.to_s,
                        " , ", vars_val.call(i.output_field, true).to_s
                    ].join("")
                end

                f = Dsl::Filter.define(ins, outs){
                    self.instance_eval(instructions.join("\n"))
                }

                return f
            end

            # @!endgroup
 
        end
    end
end