require 'bindata'
##
# Image Filter DSL Library
# (c) 2018-2020 VDTDEV/Wade H. ~ MIT License
# @author Wade H. <vdtdev.prod@gmail.com>
module ImageFilterDsl
    module Binary
        ##
        # Data structures used for storing Filter Kernels
        # in binary format
        module Struct

            ##
            # Symbols used to indicate kind of a field
            FIELD_KINDS = {
                var:            0x1a,
                literal_int:    0x12,
                literal_float:  0x1f
            }

            ##
            # Constants used in header
            HEADER_VALUES = {
                header: "ifdKernel",
                version: 0.02
            }

            ##
            # Look up hex symbol for instruction in FilterInstructions module
            # @param [Symbol] ins Instruction to look up
            # @return [Integer] Instruction hex symbol
            def self.instruction_symbol(ins)
                Dsl::FilterInstructions::OP_INS[ins]
            end

            ##
            # Image Filter DSL Field record
            class IfdField < BinData::Record
                uint16le    :symbol
                uint16le    :kind
                uint8le     :name_length
                string      :variable, :length => :name_length,
                    :onlyif => lambda { kind == FIELD_KINDS[:var] }
                int32le     :int_value,
                    :onlyif => lambda { kind == FIELD_KINDS[:literal_int] }
                float_le    :float_value,
                    :onlyif => lambda { kind == FIELD_KINDS[:literal_float] }
            end

            ##
            # Image Filter DSL Instruction record
            class IfdInstruction < BinData::Record
                uint16le    :operation
                uint8le     :input_count
                array       :input_fields, :initial_length => :input_count do
                    uint16le    :symbol
                end
                uint16le    :output_field
            end

            ##
            # Image Filter DSL Variables definition record
            class IfdVariables < BinData::Record
                uint8le     :field_count
                uint8le     :input_count
                uint8le     :output_count
                array       :definition, :initial_length => :field_count do
                    ifd_field    :field
                end
                array       :input_fields, :initial_length => :input_count do
                    uint16le    :field
                end 
                array       :output_fields, :initial_length => :output_count do
                    uint16le    :field
                end
            end

            ##
            # Image Filter DSL Header record
            class IfdHeader < BinData::Record
                string      :header, read_length: HEADER_VALUES[:header].length,
                            value: HEADER_VALUES[:header]
                float_le    :version, value: HEADER_VALUES[:version]
            end

            ##
            # Main Image Filter DSL Kernel record
            class IfdKernel < BinData::Record
                ifd_header      :header
                ifd_variables    :variables
                uint16le        :instruction_count
                array           :instructions, :initial_length => :instruction_count do
                    ifd_instruction      :instruction
                end
            end
        end
    end
end