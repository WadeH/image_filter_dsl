##
# Image Filter DSL Library
# (c) 2018-2020 VDTDEV/Wade H. ~ MIT License
# @author Wade H. <vdtdev.prod@gmail.com>
module ImageFilterDsl
    ##
    # Kernel data serializers
    module Serializers
        ##
        # Provides serialization/deserialization for Kernels
        # to an intermediate format representing kernel as hash.
        # Used by other serializers as intermediate between serializing
        # from and deserializing to kernels
        module Intermediate
            include Common
            extend self
            ##
            # Convert kernel to intermediate hash struct
            # @param [ImageFilterDsl::Dsl::Kernel] kernel Source kernel
            # @return [Hash] Intermediate hash version of kernel
            def from_kernel(kernel)
                data = {
                    inputs: kernel.inputs,
                    outputs: kernel.outputs
                }
                data[:instructions] = kernel.instructions.map do |i|
                    {op: i.op, in: i.inputs, out: i.out}
                end
                data
            end

            ##
            # Convert intermediate kernel hash to kernel object
            # @param [Hash] data Intermediate kernel data
            # @return [ImageFilterDsl::Dsl::Kernel] Constructed kernel
            def to_kernel(data)
                ins = data[:instructions].map{|i| fix_instruction_symbols(i) }
                filt = ImageFilterDsl::Dsl::Filter.define(data[:inputs],data[:outputs]) do
                    ins.each do |i|
                        send(i[:op], i[:in], i[:out])
                    end
                end
                return filt
            end

            private

            def fix_instruction_symbols(ins_hash)
                fix_hash(ins_hash,[:op,:in,:out])
                # r_k=Proc.new {|k| (ins_hash.has_key?(k))? k : k.to_s}
                # op_h = r_k.call(:op)
                # in_h = r_k.call(:in)
                # out_h = r_k.call(:out)
                # norm_sym = Proc.new {|s| (s.is_a?(String))? s.gsub(':','').to_sym : s}
                # {
                #     op: norm_sym.call(ins_hash[op_h]),
                #     in: ins_hash[in_h].map { |i|
                #         norm_sym.call(i)
                #     },
                #     out: norm_sym.call(ins_hash[out_h])
                # }
            end
        end
    end
end