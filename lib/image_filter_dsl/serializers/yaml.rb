require 'yaml'
##
# Image Filter DSL Library
# (c) 2018-2020 VDTDEV/Wade H. ~ MIT License
# @author Wade H. <vdtdev.prod@gmail.com>
module ImageFilterDsl
    module Serializers
        ##
        # Provides serialization/deserialization to/from YAML using
        # intermediate serializer
        module Yaml
            include Common
            extend self
            ##
            # Serialize kernel to YAML string
            # @param [Hash] kernel Kernel in intermediate format
            # @return [String] YAML serialized version of kernel
            def serialize(kernel)
                {
                    header: ImageFilterDsl::Binary::Struct::HEADER_VALUES,
                    inputs: kernel[:inputs],
                    outputs: kernel[:outputs],
                    instructions: kernel[:instructions]
                }.to_yaml
            end
            ##
            # Deserialize YAML kernel to intermediate format
            # @param [String] data YAML kernel data
            # @return [Hash] Intermediate format of kernel data
            def deserialize(data)
                data = YAML.load(data)
                data = fix_hash(data,[:inputs,:outputs,:instructions])
                data[:instructions] = data[:instructions].map {|i| fix_hash(i,[:op,:in,:out])}
                {
                    inputs: data[:inputs],
                    outputs: data[:outputs],
                    instructions: data[:instructions]
                }
            end
        end
    end
end