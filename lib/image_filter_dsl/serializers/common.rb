#
# Image Filter DSL Library
# (c) 2018-2020 VDTDEV/Wade H. ~ MIT License
# @author Wade H. <vdtdev.prod@gmail.com>
module ImageFilterDsl
    module Serializers
        ##
        # Common helper methods for serializers; Include in serializer modules
        # with `include Common`
        module Common
            extend self
            ##
            # Normalize symbol/value, stripping out
            # any colons and converting to symbol; Non strings
            # are returned unaltered; if array, logic is applied to all items
            # @param [String|Number|Symbol|Array] val
            # @return [Symbol|Number|Array] Normalized value
            def normalize_sym(val)
                if val.is_a?(String)
                    val.gsub(':','').to_sym
                elsif val.is_a?(Array)
                    val.map{|v|normalize_sym(v)}
                else
                    val
                end
            end
            ##
            # 'Fix' hash, converting keys to symbols (if keys provided)
            # and converting values/arrays of values so any strings are 
            # normalized and cast to symbols using {normalize_sym}
            # @param [Hash] hash Hash to fix
            # @param [Array] keys Optional array of key symbols to check for and
            #   convert string equivalents over to symbols if found
            # @return [Hash] Fixed copy of input hash
            def fix_hash(hash,keys=[])
                h2=hash.dup
                unless keys.length == 0
                    keys.each do |k|
                        if !h2.has_key?(k) && h2.has_key?(k.to_s)
                            h2[k] = h2[k.to_s]
                            h2.delete k.to_s
                        end
                    end
                end
                h2.keys.each do |k|
                    if h2[k].is_a?(Array)
                        h2[k] = h2[k].map{|v|normalize_sym(v)}
                    else
                        h2[k] = normalize_sym(h2[k])
                    end
                end
                return h2
            end
        end
    end
end

