require 'msgpack'
##
# Image Filter DSL Library
# (c) 2018-2020 VDTDEV/Wade H. ~ MIT License
# @author Wade H. <vdtdev.prod@gmail.com>
module ImageFilterDsl
    module Serializers
        ##
        # Provides serialization/deserialization to/from MessagePack strings 
        # using intermediate serializer
        module MsgPack
            include Common
            extend self
            ##
            # 'Header' added to start of array packed with MsgPack
            HEADER = ['mp',ImageFilterDsl::Binary::Struct::HEADER_VALUES.values].flatten
            ##
            # Serialize kernel to MessagePack string
            # @param [Hash] kernel Kernel in intermediate format
            # @return [String] MessagePack serialized version of kernel
            def serialize(kernel)
                [
                    HEADER,
                    kernel[:inputs],
                    kernel[:outputs],
                    kernel[:instructions].map do |i|
                        [i[:op],i[:in],i[:out]]
                    end
                ].to_msgpack
            end
            ##
            # Deserialize MessagePack kernel to intermediate format
            # @param [String] data MessagePack kernel data
            # @return [Hash] Intermediate format of kernel data
            def deserialize(data)
                data = MessagePack.unpack(data)
                head,inps,outs,ins=data
                d = {
                    inputs: normalize_sym(inps),
                    outputs: normalize_sym(outs),
                    instructions: ins.map do |i|
                        op,iin,iout=i
                        fix_hash({op: op, in: iin, out: iout})
                    end
                }
                
                return d
            end
        end
    end
end