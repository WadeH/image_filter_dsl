require 'fileutils'
require './lib/image_filter_dsl.rb'

module DataHelper
    extend self
    
    DIR = 'tmp'

    Kernel = ImageFilterDsl::Dsl::Filter.define([:r],[:g]) do
        max [:r, 0.75], :r2
        avg [:r, :r2], :r
    end


    def path()
        File.join('.',RSpec.configuration.default_path,DIR)
    end

    def reset()
        p = path
        FileUtils.remove_dir(p,true)
        FileUtils.mkdir(p)
    end

end