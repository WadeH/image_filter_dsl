require './lib/image_filter_dsl/dsl/filter_instructions.rb'
require './lib/image_filter_dsl/binary/struct.rb'

RSpec.describe ImageFilterDsl::Binary::Struct do

    # Struct = ImageFilterDsl::Binary::Struct

    it "instruction_symbol returns correct hex" do
        ins = ImageFilterDsl::Dsl::FilterInstructions::OP_INS

        ins.keys.each do |k|
            expect(
                ImageFilterDsl::Binary::Struct.instruction_symbol(k)
            ).to eq ins[k]
        end
    end
    
end