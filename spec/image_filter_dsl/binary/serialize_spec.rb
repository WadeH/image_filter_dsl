require './lib/image_filter_dsl/dsl/filter.rb'
require './lib/image_filter_dsl/dsl/filter_instructions.rb'
require './lib/image_filter_dsl/dsl/kernel.rb'
require './lib/image_filter_dsl/binary/struct.rb'
require './lib/image_filter_dsl/binary/serialize.rb'

RSpec.describe ImageFilterDsl::Binary::Serialize do

    Serial = ImageFilterDsl::Binary::Serialize

    def sample_filter
        ImageFilterDsl::Dsl::Filter.define [:x,:y], [:r,:g] do
            add [:x, :y], :r
            avg [:x, :y], :t
            mult [:t, -1], :t
            max [:x, :y], :t2
            add [:t2, :t], :g
        end
    end

    context "Record from Kernel with from_kernel" do
        it "handles input and output variables" do
            f = sample_filter()
            r = Serial.from_kernel(f)

            expect(r.variables.input_count).to eq(f.inputs.length)
            expect(r.variables.output_count).to eq(f.outputs.length)

            def_vars = r.variables.definition.map{|v|v[:variable]}
            f.inputs.each do |i|
                expect(def_vars.include?(i.to_s)).to be true
            end
            f.outputs.each do |i|
                expect(def_vars.include?(i.to_s)).to be true
            end
        end
        it "handles instruction ops and inputs" do
            f = sample_filter()
            r = Serial.from_kernel(f)

            expect(r.instruction_count).to eq(f.instructions.length)
            
            def_ins = r.instructions.map{|i|{op: i[:operation], inputs: i[:input_count]}}
            
            struct = ImageFilterDsl::Binary::Struct

            f.instructions.each_with_index do |ins, i|
                expect(
                    r.instructions[i].operation
                ).to eq struct.instruction_symbol(ins.op)
                expect(
                    r.instructions[i].input_count
                ).to eq ins.inputs.length
            end
        end
    end

    context "Kernel from Record with from_record" do
        it "handles input and output variables" do
            fo = sample_filter()
            r = Serial.from_kernel(fo)
            f = Serial.from_record(r)

            expect(f.inputs.length).to eq fo.inputs.length
            expect(f.outputs.length).to eq fo.outputs.length

            expect(f.inputs).to match_array(fo.inputs)
            expect(f.outputs).to match_array(fo.outputs)
        end
        it "handles instruction ops and inputs" do
            fo = sample_filter()
            r = Serial.from_kernel(fo)
            f = Serial.from_record(r)

            f.instructions.each_with_index do |ins,i|
                expect(ins.op).to be fo.instructions[i].op
                expect(ins.inputs).to match_array(fo.instructions[i].inputs)
                expect(ins.out).to be fo.instructions[i].out
            end
        end
    end
    
end