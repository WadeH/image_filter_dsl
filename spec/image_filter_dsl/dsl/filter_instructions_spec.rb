require './lib/image_filter_dsl/dsl/filter.rb'
require './lib/image_filter_dsl/dsl/filter_instructions.rb'
require './lib/image_filter_dsl/dsl/kernel.rb'

RSpec.describe ImageFilterDsl::Dsl::FilterInstructions do

    Filt = ImageFilterDsl::Dsl::Filter

    context "Math operation" do
        it "add" do
            f = Filt.define [:a], [:b] do
                add [:a, 2], :b
            end

            o = f.process({a: 2})

            expect(o[:b]).to eq(4)
        end

        it "mult" do
            f = Filt.define [:a], [:b] do
                mult [:a, 3], :b
            end

            o = f.process({a: 2})

            expect(o[:b]).to eq(6)
        end

        it "div" do
            f = Filt.define [:a], [:b] do
                div [6,:a], :b
            end

            o = f.process({a: 2})

            expect(o[:b]).to eq(3)
        end
    end

    context "Collection operation" do
        it "min" do
            f = Filt.define [:a], [:b] do
                min [:a, 3], :b
            end

            o = f.process({a: 2})

            expect(o[:b]).to eq(2)
        end

        it "max" do
            f = Filt.define [:a], [:b] do
                max [:a, 3], :b
            end

            o = f.process({a: 2})

            expect(o[:b]).to eq(3)
        end

        it "avg" do
            f = Filt.define [:a], [:b] do
                avg [:a,4], :b
            end

            o = f.process({a: 2})

            expect(o[:b]).to eq(3)
        end
    end

    context "Logic/Memory/Conversion operation" do
        it "copy" do
            f = Filt.define [:a], [:b] do
                copy [:a], :b
            end

            o = f.process({a: 2})

            expect(o[:b]).to eq(2)
        end

        it "above" do
            f1 = Filt.define [:a], [:b] do
                above [:a, 1, 1, 0], :b
            end
            f2 = Filt.define [:a], [:b] do
                above [:a, 3, 1, 0], :b
            end

            o1 = f1.process({a: 2})
            o2 = f2.process({a: 2})

            expect(o1[:b]).to eq(1)
            expect(o2[:b]).to eq(0)
        end

        it "below" do
            f1 = Filt.define [:a], [:b] do
                below [:a, 3, 1, 0], :b
            end
            f2 = Filt.define [:a], [:b] do
                below [:a, 1, 1, 0], :b
            end

            o1 = f1.process({a: 2})
            o2 = f2.process({a: 2})

            expect(o1[:b]).to eq(1)
            expect(o2[:b]).to eq(0)
        end

        it "floor" do
            f = Filt.define [:a], [:b] do
                floor [:a], :b
            end

            o = f.process({a: 2.5})

            expect(o[:b]).to eq(2)
        end

        it "ceil" do
            f = Filt.define [:a], [:b] do
                ceil [:a], :b
            end

            o = f.process({a: 2.5})

            expect(o[:b]).to eq(3)
        end

        it "float" do
            f = Filt.define [:a], [:b] do
                float [:a], :b
            end

            o = f.process({a: 2})

            expect(o[:b]).to eq(2.0)
        end

        it "round" do
            f = Filt.define [:a], [:b] do
                round [:a,2], :b
            end

            o = f.process({a: 2.5432})

            expect(o[:b]).to eq(2.54)
        end

        it "mix" do
            f = Filt.define [:a,:b], [:c] do
                mix [0.5, :a, :b], :c
            end

            o =f.process({a: 6, b: 12})

            expect(o[:c]).to eq(3 + 6)
        end
    end

    context "generator operations" do
        it "rand" do
            f = Filt.define [:a], [:b] do
                rand [-5,5], :b
            end

            o =f.process({a:0})

            expect(o[:b].to_i).to be >= -5
            expect(o[:b].to_i).to be <= 5
        end
        it "sin" do
            inp = 1.5
            f = Filt.define [:a], [:b] do
                sin [:a], :b
            end

            o =f.process({a:inp})

            expect(o[:b]).to be(Math.sin(inp))
        end
        it "cos" do
            inp = 1.5
            f = Filt.define [:a], [:b] do
                cos [:a], :b
            end

            o =f.process({a:inp})

            expect(o[:b]).to be(Math.cos(inp))
        end
        it "tan" do
            inp = 1.5
            f = Filt.define [:a], [:b] do
                tan [:a], :b
            end

            o =f.process({a:inp})

            expect(o[:b]).to be(Math.tan(inp))
        end
    end

end