require './lib/image_filter_dsl/dsl/filter.rb'
require './lib/image_filter_dsl/dsl/filter_instructions.rb'
require './lib/image_filter_dsl/dsl/kernel.rb'

RSpec.describe ImageFilterDsl::Dsl::Filter do

    def sample_filter
        ImageFilterDsl::Dsl::Filter.define [:x,:y], [:r,:g] do
            add [:x, :y], :r
            avg [:x, :y], :t
            mult [:t, -1], :t
            max [:x, :y], :t2
            add [:t2, :t], :g
        end
    end

    context "Define filter" do
        it "accepts inputs and outputs" do
            f = sample_filter()

            expect(f.inputs).to contain_exactly(:x,:y)
            expect(f.outputs).to contain_exactly(:r, :g)
        end

        it "contains instructions with correct ops in order" do
            f = sample_filter()

            ops = f.instructions.map{|i| i.op}
            expect(ops).to contain_exactly(
                :add, :avg, :mult, :max, :add
            )
        end

        it "process gives expected result" do
            f = sample_filter()

            p = f.process({x: 3, y: 2})
            
            expect(p[:r]).to eq(5)
            expect(p[:g]).to eq(0.5)
        end
    end
end