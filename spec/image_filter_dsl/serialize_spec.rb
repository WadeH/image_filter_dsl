require 'spec_helper'
require 'pry'
RSpec.describe ImageFilterDsl::Serialize do

    ser = ImageFilterDsl::Serialize

    context "Serializer modules" do
        it "resolve to defined modules" do
            ser::FORMATS.each do |f|
                md = defined? ser::SERIALIZERS[f]
                expect(md).not_to be_nil
            end
        end
        it "has valid intermediate module" do
            md = ser::INTERMEDIATE
            expect(md).not_to be_nil
        end
    end

    context "Serializer module serialize/deserialize" do
        ser::FORMATS.each do |f|
            context "for format #{f}" do
                kernel = DataHelper::Kernel.dup
                kser = ser.from_kernel(kernel,f)
                kdes = ser.to_kernel(kser,f)
                it "maintains inputs" do
                    expect(kdes.inputs).to match_array(kernel.inputs)
                end
                it "maintains outputs" do
                    expect(kdes.outputs).to match_array(kernel.outputs)
                end
                it "maintains instructions with correct ops" do
                    kdes.instructions.each_with_index do |ins,i|
                        expect(ins.op).to eq(kernel.instructions[i].op)
                    end
                end
            end
        end
    end
   
end