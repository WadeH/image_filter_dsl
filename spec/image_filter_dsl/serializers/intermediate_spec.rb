require 'spec_helper'
# require './lib/image_filter_dsl.rb'
# require 'pry'
RSpec.describe ImageFilterDsl::Serializers::Intermediate do

    ser = ImageFilterDsl::Serializers::Intermediate

    before(:all) do
        @fkern = DataHelper::Kernel
        # ImageFilterDsl::Engine::IO.write(
        #     File.join(TmpData.path,'tmp.ifdk')
        # )
    end

    after(:each) do
        DataHelper.reset
    end

    context "to_kernel" do
        before(:each) do
            @fdata = ser.from_kernel(@fkern)
            @fkern2 = ser.to_kernel(@fdata)
        end
        it "has inputs" do
            expect(@fkern2.inputs).to match_array(@fdata[:inputs])
        end
        it "has outputs" do
            expect(@fkern2.outputs).to match_array(@fdata[:outputs])
        end
        it "has instructions with correct ops" do
            @fkern2.instructions.each_with_index do |ins,idx|
                expect(ins.op).to eq(@fdata[:instructions][idx][:op])
            end
        end
    end

    context "from_kernel" do
        # binding.pry
        # TODO
        
        it "has inputs" do
            d = ser.from_kernel(@fkern)
            expect(d[:inputs]).to match_array(@fkern.inputs.to_a)
        end
        it "has outputs" do
            d = ser.from_kernel(@fkern)
            expect(d[:outputs]).to match_array(@fkern.outputs.to_a)
        end
        it "has instructions with correct ops" do
            d = ser.from_kernel(@fkern)
            d[:instructions].each_with_index do |ins,idx|
                expect(ins[:op]).to eq(@fkern.instructions[idx].op)
            end
        end
    end
end