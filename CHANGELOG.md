# @title Change Log
# Image Filter DSL Change Log

_(started 2020/02/03, previous version 0.0.5)_

__2020-04-12__ _(0.1.1-0.1.3)_

- Rewrote CLI interface to have logic within gem and to be more expandable in the future
- Added serializers for YAML, JSON and MsgPack
- Added Rake tasks for building gem and docs
- _Extra version bumps because of forgotten doc updates, and forgetten dependency addition_

__2020-03-06__ _(0.1.0)_

- Updated `below` operator to allow 2 inputs, defaulting the missing 2 (`true val` and `false val`) to `1` and `0` respectively
- Updated `above` operator to allow 2 inputs, defaulting the missing 2 (`true val` and `false val`) to `1` and `0` respectively
- Added `band` and `bor` operators
- Added `between` operator
- Added `clamp` operator
- Added README entry for `floor`, which was missing
- Bumped default kernel header version value from `0.01` to `0.02`
- Added `weighted_mix` sample filter

__2020-02-03__ _(0.0.6)_

- Added `mix` operator
- Added `rand` operator
- Added trig operators `sin`, `cos` and `tan`
- Added `rand_fade` sample filter

