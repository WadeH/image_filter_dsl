##
# Pass result of various filter sample methods (filters)
# into test method along with name to use when
# saving binary kernal and when writing result image spec_(name)
module Samples
    Filter = ImageFilterDsl::Filter
    def self.rect_grad() 
        Filter.define [:x,:y,:width,:hght], [:r, :g, :b, :a] do
            copy [255], :a
            max [0.1, :x], :t
            float [:t], :t
            div [:t, :width], :t2
            mult [255, :t2], :t2
            copy [:t2], :r
            max [0.1, :y], :t
            float [:t], :t
            div [:t, :hght], :t3
            mult [255, :t3], :t3
            copy [:t3], :g
            avg [:t2, :t3], :t
            copy [:t], :b
        end
    end

    def self.trans_rect_grad() 
        Filter.define [:x,:y,:width,:hght, :a], [:r, :g, :b, :a] do
            copy [:a], :a
            max [0.1, :x], :t
            float [:t], :t
            div [:t, :width], :t2
            mult [255, :t2], :t2
            copy [:t2], :r
            max [0.1, :y], :t
            float [:t], :t
            div [:t, :hght], :t3
            mult [255, :t3], :t3
            copy [:t3], :g
            avg [:t2, :t3], :t
            copy [:t], :b
        end
    end

    def self.line_vert_inv()
        Filter.define [:y, :r, :g, :b, :a], [:r, :g, :b, :a] do
            copy [:a], :a
            mod [:y, 10], :t1
            above [:t1, 3, 1.0, 0.0], :cond
            add [-255,:b], :invb
            add [-255,:g], :invg
            add [-255,:r], :invr
            abs [:invb], :invb
            abs [:invg], :invg
            abs [:invr], :invr
            switch [:cond, :r, :invr], :r
            switch [:cond, :g, :invg], :g
            switch [:cond, :b, :invb], :b
        end
    end

    def self.avg_enhance_red()
        Filter.define [:r,:g,:b,:a], [:r,:g,:b,:a] do
            copy [:a], :a
            # avg g + b, * 2, cap to max of 255
            avg [:g,:b], :tavg
            mult [:tavg, 2], :tavg
            min [:tavg,255], :tavg
            max [:tavg, :r], :tavg
            max [:g, :b], :mgb
            above [:r, :mgb,1,0], :cond
            mult [:g, 0.5], :tg
            mult [:b, 0.5], :tb
            switch [:cond, :tg, :g], :g
            switch [:cond, :tb, :b], :b
            switch [:cond, :tavg, :r], :r
        end
    end

    def self.exagerate()
        Filter.define [:r,:g,:b,:a],[:r,:g,:b,:a] do
            avg [:r,:g], :avb
            avg [:g,:b], :avr
            avg [:b, :r], :avg
            above [:avb, :b], :cb
            above [:avr, :r], :cr
            above [:avg, :g], :cg
            mult [:avb, 2], :eb
            mult [:avr, 2], :er
            mult [:avg, 2], :eg
            min [:eb, 255], :eb
            min [:er, 255], :er
            min [:eg, 255], :eg
            mult [:r, 0.5], :hr
            mult [:g, 0.5], :hg
            mult [:b, 0.5], :hb
            switch [:cr, :er, :hr], :r
            switch [:cg, :eg, :hg], :g
            switch [:cb, :eb, :hb], :b
            copy [:a], :a
        end
    end

    def self.exagerate2()
        Filter.define [:r,:g,:b,:a],[:r,:g,:b,:a] do
            avg [:r,:g], :avb
            avg [:g,:b], :avr
            avg [:b, :r], :avg
            below [:avb, :b], :cb
            below [:avr, :r], :cr
            below [:avg, :g], :cg
            mult [:avb, 2], :eb
            mult [:avr, 2], :er
            mult [:avg, 2], :eg
            min [:eb, 255], :eb
            min [:er, 255], :er
            min [:eg, 255], :eg
            switch [:cr, :er, :r], :r
            switch [:cg, :eg, :g], :g
            switch [:cb, :eb, :b], :b
            copy [:a], :a
        end
    end

    def self.exagerate3()
        Filter.define [:r,:g,:b,:a],[:r,:g,:b,:a] do
            max [:r, :g], :m
            max [:m, :b], :m
            eq [:m, :r], :ruse
            eq [:m, :g], :guse
            eq [:m, :b], :buse
            avg [:r,:g], :avb
            avg [:g,:b], :avr
            avg [:b, :r], :avg
            below [:avb, :b], :cb
            below [:avr, :r], :cr
            below [:avg, :g], :cg
            mult [:avb, 2], :eb
            mult [:avr, 2], :er
            mult [:avg, 2], :eg
            min [:eb, 255], :eb
            min [:er, 255], :er
            min [:eg, 255], :eg
            switch [:cr, :er, :r], :rx
            switch [:cg, :eg, :g], :gx
            switch [:cb, :eb, :b], :bx
            switch [:ruse, :rx, :r], :r
            switch [:guse, :gx, :g], :g
            switch [:buse, :bx, :b], :b
            copy [:a], :a
        end
    end
    
    def self.rand_fade()
        Filter.define [:y,:hght,:x,:width,:h, :r,:g],[:r,:g] do
            max [0.1, :y], :typ
            max [0.1, :x], :txp
            float [:typ], :typ
            float [:txp], :txp
            div [:typ, :hght], :typ
            div [:txp, :width], :txp
            rand [0, 255], :rr      # Random number between 0...255
            rand [0, :g], :rg       # Random number between 0 ... green
            mix [:typ, :rr, :r], :r # Mix random color with r color (ratio by y position)
            mix [:txp, :rg, :g], :g # Mix random color with g color (ratio by x position)
        end
    end

    def self.weighted_mix()
        Filter.define [:r,:g,:b,:y,:hght],[:r,:g,:b] do
            above [:r,:g], :bT
            above [:r,:b], :gT
            above [:b,:g], :rT
            # max [:r,:g], :bMrg
            # max [:r,:b], :gMrb
            # max [:b,:g], :rMbg
            switch [:bT, :r, 1], :bM
            switch [:gT, :r, 1], :gM
            switch [:rT, :b, 1], :rM
            div [:b, :bM], :nB
            div [:g, :gM], :nG
            div [:r, :rM], :nR
            max [0.1, :y], :typ
            float [:typ], :typ
            div [:typ, :hght], :typ
            avg [:b,:nB], :nB
            avg [:g,:nG], :nG
            avg [:r,:nR], :nR
            mult [:nB, :bT], :bT
            mult [:nG, :gT], :gT
            mult [:nR, :rT], :rT
            add [:nB,:bT], :nB
            add [:nG,:gT], :nG
            add [:nR,:rT], :nR
            mix [:typ, :r, :nR], :r
            mix [:typ, :g, :nG], :g
            mix [:typ, :b, :nB], :b
        end
    end

    def self.weighted_rand()
        Filter.define [:r,:g,:b,:y,:hght],[:r,:g,:b] do
            # above [:r,:g], :bT
            # above [:r,:b], :gT
            # above [:b,:g], :rT
            max [:r,:g], :bMrg
            max [:r,:b], :gMrb
            max [:b,:g], :rMbg
            float [:bMrg], :bMrg
            float [:gMrb], :gMrb
            float [:rMbg], :rMbg
            div [:bMrg,255.0], :bW
            div [:gMrb,255.0], :gW
            div [:rMbg,255.0], :rW
            rand [0,255], :r0
            rand [0,255], :r1
            rand [0,255], :r2
            mult [:bW,:r0], :r0
            mult [:gW,:r1], :r1
            mult [:rW,:r2], :r2
            avg [:r0,:r],:r
            avg [:r1,:g],:g
            avg [:r2,:b],:b
        end
    end
    
    ##
    # Run filter
    # @param [ImageFilterDsl::Filter] filter Filter to run
    # @param [String] name Name to use when writing filter kernal and output image
    def self.test(filt,name=nil)
        ImageFilterDsl.save_binary_kernel(filt,"./sample_filters/#{name}.ifdk")
        p = ImageFilterDsl.image_processor(filt)
        p.process_image("./sample_filters/spec.png", "./sample_filters/#{name}_spec.png")
    end
end