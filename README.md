# Image Filter DSL

_An Image Filter DSL (duh)_

[![Gem Version](https://badge.fury.io/rb/image_filter_dsl.svg)](https://badge.fury.io/rb/image_filter_dsl)

{file:CHANGELOG.md Change Log}

## Features

- DSL with customizable input and output variables, and a set of basic instructions
- IO with serialization so Kernels can be written to binary files and loaded from binary files
- Image Processor that generates image file by applying Filter Kernel to every pixel of input image (supports multi-threading)

## DSL

### Filter Block

Using `ImageFilterDsl::Dsl::Filter`

```ruby
# - First array contains input variables
# x, y, r, g, b, a, :width and :hght are automatically
# populated by image filter process
# - Second array contains output vars; r,g,b,a and optionally
# x,y for images
filter = Filter.define [:x,:y,:r,:g,:b], [:r,:g,:b] do 
    # Instructions go here
    # instruction [input(s)], output
    add [:x,:r], :g

    # output can be an existing symbol in out, or a custom one up to 5 letters 
    # long (longer will be trimmed if serialized to a binary file)

    # input can be any declared input or custom variable
end
```

### Instructions

Basic Use:

``instruction [argument1,argument2,...], :output_variable``

__Math__

- `add` add 2 or more input values, storing in output (<code>a+b</code>)

    ```ruby
    add [:r,5], :r # replace r with r + 5
    ```

- `mult` add 2 or more inputs, storing in out (<code>a*b</code>)

    ```ruby 
    mult [:r,2], :r # Replace r with r * 2
     ```

- `div` divide 2 inputs

    ```ruby
    div [:r, 2], :g # Write r/2 to g
    ```

- `mod` store modulo of two inputs in output

    ```ruby
        mod [2,4], :t # Write 2$4 (2) to t
    ```

- `abs` store absolute value of input in output

    ```ruby
        abs [-5], :r # Writes abs(5) to r
    ```

__Collection__

- `min` store smallest of inputs in output (accepts 2 or more values)

    ```ruby
        min [:r,255,:g], :s # Writes smallest of r, 255 and g to s
    ```

- `max` store largest of inputs in output (accepts 2 or more values)

    ```ruby
        max [:r,0,:g], :s # Writes largest of r, 0 and g to s
    ```

- `avg` store average of inputs in output (accepts 2 or more values)

    ```ruby
        avg [:r,:g,:b], :ca # Writes (r+g+b)/3 to ca
    ```


__Logic/Conversion/Memory__

- `above` choose one of two values based on greater than comparison (2 or 4 values) `[n1,n1,true_val,false_val]` (If not specified, `true_val` is 1 and `false_val` is 0)

    ```ruby
        # if r > g, store 0 in g, else store g in g (keep g the same)
        above [:r,:g,0,:g], :g
    ```

- `below` choose one of two values based on less than comparison (2 or 4 values) `[n1,n1,true_val,false_val]` (If not specified, `true_val` is 1 and `false_val` is 0)

    ```ruby
        # if r < g, store 0 in r, else store r in r (keep r the same)
        below [:r,:g,0,:r], :r
    ```

- `between` choose one of two values based on whether value is between two others (3 or 5 values) `[min,max,value,true_val,false_val]` (If not specified, `true_val` is 1 and `false_val` is 0)

    ```ruby
        # Returns value since val > 1 && val < 3
        below [1,3,2], :r
        # Returns 1 since -1 < 1
        below [1,-1,5], :r
    ```

- `switch` choose one of two values based on condition value (1/1.0 = true, 0/0.0 = false) `[cond,true_val,false_val]`

    ```ruby
        # If c is true, store r in a, else store g in a
        switch [:c,:r,:g], :a
    ```

- `eq` if two input values are equal, store 1, else 0 (_2 values_) (`[val1,val2]`)
- `bnot` if input is 1 or 1.0, store 0, else 1 (_1 value_) (`[bool]`)
- `band` if _all_ input values are 1 or 1.0, store 1, else 0 (_1+ values_) (`[val1,val2,...]`)
- `bor` if _any_ input values are 1 or 1.0, store 1, else 0 (_1+ values_) (`[val1,val2,...]`)
- `copy` copy input directly to output (`[src]`)
- `ceil` nearest whole integer of input, rounded up (_1 value_) (`[val]`)
- `floor` nearest whole integer of input, rounded down (_1 value_) (`[val]`)
- `clamp` input clamped to be no less than min, no greater than max (`[min,max,val]`)
- `float` cast input to float (_1 value_) (`[val]`)
- `round` round first input value to second value decimals (`[value,decimal_count]`)
- `mix` Mix two values together with a ratio (0*a + (1-r)*b) (`[ratio,a,b]`)

    ```ruby
        # store (0.3*r) + (0.7*g) in b
        mix [0.3,:r,:g], :b
    ```

__Generators__

- `rand` random float between min and max (`[min,max]`)
- `sin` sine function on single value
- `cos` cosine function on single value
- `tan` tangent function on single value

### Sample

Define filter

```
    swizzle = Filter.define [:r,:g,:b], [:r,:g,:b] do
        copy [:g], :t # copy green to temp
        copy [:r], :g # copy red to green
        copy [:b], :r # copy blue to red
        copy [:t], :b # copy temp (original green) to blue
    end
```

Optionally write filter kernal to binary file on disk

```ruby
    ImageFilterDsl::Engine::IO.write("./swizzle.ifdk", swizzle)
```

Use filter kernal to process image

```ruby
    # From binary kernal file
    processor = ImageFilterDsl::Engine::ImageProcessor.new('./swizzle.ifdk')
    # OR from filter object
    processor = ImageFilterDsl::Engine::ImageProcessor.new(swizzle)
    # or use aliases in main module
    processor = ImageFilterDsl.image_processor(swizzle)

    # Process image and store output
    processor.process_image('./my_source.png', './my_output.png')
```

- See `./sample_filters/samples.rb` for sample filters
- See Also: [image_filter_dsl_samples](https://bitbucket.org/WadeH/image_filter_dsl_samples/src/master/) for more samples

## Gem

Either

- Build into a gem using included `gemspec` file; includes CLI functionality
- Install using gem with `gem install image_filter_dsl`

## CLI

Image Filter DSL can be made to process an image from a binary kernal using
its CLI tool, `image_filter_dsl`

- Usage: `image_filter_dsl --process <kernel_file> <image_in> <image_out>`

## License

(c) 2018-2020, Wade H. (vdtdev.prod@gmail.com). All Rights Reserved.
Released under MIT license