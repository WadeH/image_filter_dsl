version = File.read('./.version').strip
Gem::Specification.new do |s|
    s.name          = 'image_filter_dsl'
    s.version       =  version
    s.date          = '2020-03-06'
    s.summary       = "Image Filter DSL"
    s.description   = <<-eof 
        Image filtering DSL including a CLI for running filters and ability
        to write kernals to a binary format that will can be used to run filters
        from the CLI, and potentially allow filters to be compatible with implementation
        in other languages.
    eof
    s.authors       = ["Wade H."]
    s.email         = 'vdtdev.prod@gmail.com'
    s.files         = ["lib/image_filter_dsl.rb"]
    s.files         += Dir['lib/image_filter_dsl/**/*.rb']
    s.files         << 'README.md'
    s.executables   << 'image_filter_dsl'
    s.license       = 'MIT'
    s.homepage      = "https://bitbucket.org/WadeH/image_filter_dsl"
    s.metadata      = {
        "source_code_uri" => "https://bitbucket.org/WadeH/image_filter_dsl",
        "documentation_uri" => "https://rubydoc.info/gems/image_filter_dsl/#{version}"
    }
    s.add_runtime_dependency 'chunky_png', '~> 1.3', '>= 1.3.10'
    s.add_runtime_dependency 'bindata', '~> 2.4', '>= 2.4.3'
    s.add_development_dependency 'rspec', '~> 3.7', '>= 3.7.0'
    s.add_runtime_dependency 'msgpack', '~> 1.3', '>= 1.3.3'
end